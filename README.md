# Python数据分析

#### 介绍
一个简单的数据分析项目，看起来高大上。

关于B站弹幕数据分析的项目。

文件有点多，所以弄了个压缩包。

#### 技术使用

- 爬虫，初级爬虫，简单的demo。
- 数据分析。
- pyecharts完成数据分析图表化。
- flask网页展示分析结果。

#### 使用说明
解压完成后，将项目导入pycharm，运行命令：pip install -r requirements.txt -i https://pypi.doubanio.com/simple，即可加载项目所需要的依赖包，就可以运行了。


#### 压缩包文件

![输入图片说明](https://images.gitee.com/uploads/images/2020/0909/141934_a021edee_5098870.png "屏幕截图.png")

#### 运行截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0909/142810_e0366a98_5098870.png "屏幕截图.png")
